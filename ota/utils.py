from uos import rename, rmdir, remove, mkdir, listdir, ilistdir
from gc import collect
#import ota.requests as requests
import urequests

def ensure_dirs(path):
	split_path = path.split('/')
	if len(split_path) > 1:
		for i, fragment in enumerate(split_path):
			parent = '/'.join(split_path[:-i])
			try:
				mkdir(parent)
			except OSError:
				pass


def move_f(source, destination):
	"""Move file or directory with subfiles"""
	# for isdir in ilistdir(source):
		# if isdir[1] is 16384: #Es un directorio
			# try:
				# content = listdir(source)
			# except OSError:
				# content = None
			# for item in content: #Es un directorio
				# if not '.' in item and not item in listdir(destination):
					# print("Creating directory %s in %s" %(item,destination))
					# mkdir('{}/{}'.format(destination, item))
				# print("moving %s to %s" %('{}/{}'.format(source, item), '{}/{}'.format(destination, item)))
				# move_f('{}/{}'.format(source, item), '{}/{}'.format(destination, item))
				# if not '.' in item and not listdir('{}/{}'.format(source, item)):
					# print("Deleting empty folder %s in %s" %(item,source))
					# rmdir('{}//{}'.format(source, item))
		# if isdir[1] is 32768: #Es un archivo
			# print("Renaming %s to %s" %(source, destination))
			# rename(source, destination)
			# return
	
	try:
		content = listdir(source)
	except OSError:
		content = None

	if not content: #Es un archivo
		print("Renaming %s to %s" %(source, destination))
		rename(source, destination)
		return

	for item in content: #Es un directorio
		if not '.' in item and not item in listdir(destination):
			print("Creating directory %s in %s" %(item,destination))
			mkdir('{}/{}'.format(destination, item))
		print("moving %s to %s" %('{}/{}'.format(source, item), '{}/{}'.format(destination, item)))
		move_f('{}/{}'.format(source, item), '{}/{}'.format(destination, item))
		if not '.' in item and not listdir('{}/{}'.format(source, item)):
			print("Deleting empty folder %s in %s" %(item,source))
			rmdir('{}//{}'.format(source, item))
		
def download_file(url, save_path):
	collect()
	response = urequests.get(url)
	try:
		dir_index= save_path.rindex("/")
		rute=save_path[:dir_index] #Eliminacion de nombre de archivo en la ruta
#		print(rute)
		dir_index=rute.rindex("/")
		dir_rute=rute[:dir_index]#Eliminacion de nombre de carpeta contenedora de archivo a descargar en la ruta
		name_folder=rute[dir_index+1:]
#	print(save_path)
		if not name_folder in listdir(dir_rute):
			print("Creating directory %s in %s" %(name_folder,dir_rute))
			mkdir(rute)
#		print(rute)
	except Exception as e:
		pass
	with open(save_path, 'wb') as target_file:
		target_file.write(response.content)
#	gc.mem_free()

def verify_empty(source):
	pass

def purge_folder(source):
	for isdir in ilistdir(source):
		if isdir[1] is 16384:
			typ ="folder"
			try:
				content = listdir(source)
			except OSError:
				content = None
			if not content:
				rmdir(source)
			else:
				purge_folder('{}/{}'.format(source, isdir[0]))
				rmdir('{}/{}'.format(source, isdir[0]))
		if isdir[1] is 32768:
			remove('{}/{}'.format(source, isdir[0]))
			typ="file"
		print("Deleting %s %s " %(typ,'{}/{}'.format(source, isdir[0]))) 