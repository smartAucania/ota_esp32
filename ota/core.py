import urequests
import uos
import ujson
from gc import collect
#import ota.requests as requests
from ota.utils import move_f, download_file, ensure_dirs, purge_folder

PROVIDERS = {
	'github': 'https://api.github.com/repos/{owner}/{repo}',
}

class Firmware(object):
	def __init__(self, verbose=2):
		self.verbose = verbose

		self.version = self.__get_version()
		self.conf = self.__get_conf()

		self.__base_url = self.__get_base_url()

	def __get_version(self):
		version=self.read_file('firmware/.version')
#		try:
#			with open('firmware/.version') as version_file:
#				version = version_file.read()
#		except OSError:
#			version = None
		print("Local version: %s" %version)
		return version

	def __get_conf(self):
		with open('ota_conf.json') as conf_file:
			conf_json = conf_file.read()

		return ujson.loads(conf_json)

	def __get_base_url(self):
		provider = self.conf['remote']['provider']
		base_url = PROVIDERS.get(provider.lower())

		if not base_url:
			raise ValueError('{} is not supported. '
							 'Available providers: {}'.format(provider, ', '.join(PROVIDERS.keys())))
		base_url = base_url.format(**self.conf['remote'])
		return base_url

	def __request_get(self, url, headers=None):
		default_headers = {
			'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) '
						  'AppleWebKit/537.36 (KHTML, like Gecko) '
						  'Chrome/50.0.2661.102 Safari/537.36',
		}
		if headers:
			default_headers.update(headers)
		return urequests.get(url, headers=default_headers)

	def remote_version(self):
		endpoint = '/commits/master'
		headers = {'Accept': 'application/vnd.github.VERSION.sha'}
		attempt=0
		response = None
		while response is None or attempt < 3:
			response = self.__request_get(self.__base_url + endpoint, headers=headers)
			if response.status_code != 200:
				raise ValueError('Error while getting version from server '
								 '"{}: {} ({})"'.format(response.status_code, response.reason, response.text))
			attempt =+1
		print("Remote version: %s" %response.text.strip())
		return response.text.strip()

	def has_latest_version(self):
		return self.version == self.remote_version()

	def __download_latest(self):
		target = 'firmware'
		try:
			purge_folder(target)
			uos.rmdir(target)
		except OSError:
			pass
		collect()
		uos.mkdir(target)
		self.__download_from_git(git_dir=self.conf['remote'].get('root_dir', ''), target_dir=target)

	def __download_from_git(self, git_dir='', target_dir='firmware'):
		endpoint = '/contents/' + git_dir
		response = self.__request_get(self.__base_url + endpoint)
		paths_data = response.json()
		for path in paths_data:
			if path['download_url']:
				if self.verbose > 1:
					print('Downloading {}...'.format(path['path']))
				download_file(path['download_url'], '{}/{}'.format(target_dir, path['path']))
			else:
				ensure_dirs('{}/{}'.format(target_dir, path['path']))
				self.__download_from_git(path['path'], target_dir=target_dir)

	@staticmethod
	def backup(dir_to_backup='firmware'):
		target = 'backup'
		try:
			purge_folder(target)
			uos.rmdir(target)
		except Exception as e:
			print(repr(e))
		uos.mkdir(target)
		move_f(dir_to_backup, target)

	@staticmethod
	def restore_backup(dir_to_restore='firmware'):
		target = 'backup'
		uos.chdir('/')
		try:
			purge_folder(dir_to_restore)
			uos.rmdir(dir_to_restore)
#		except OSError:
		except Exception as e:
			print(repr(e))
#			pass
		uos.mkdir(dir_to_restore)
		move_f(target, dir_to_restore)

	def update(self, schedule=False,force=False):
		print("Force: %d Schedule: %d" %(force, schedule))
		self.version = self.__get_version()
		if not force and self.has_latest_version():
			if self.verbose > 0:
				print('Firmware already up to date')# (version: {})'.format(self.version))
			return False
		if self.verbose > 1 and schedule is True:
			self.schedule_update()
			return True
		if self.verbose > 1:
			if self.version is not None:
				print('Storing backup of current firmware version...')
				Firmware.backup()
			print('Downloading new firmware...')
		try:
			purge_folder('firmware')
			uos.rmdir('firmware')
		except OSError:
			pass
		self.__download_latest()
		self.create_ver_file()
		
		if self.verbose > 1:
			print('New firmware is installed.')
		collect()

	def create_ver_file(self):
		self.write_file('firmware/.version', self.remote_version())
#		with open('firmware/.version', 'w') as target_file:
#			target_file.write(self.remote_version())

	def verify(self):
		uos.chdir("/")
		target = 'firmware'
		content= uos.listdir(target)
		if not content:
			uos.rmdir(target)
			return False
		return True
	def write_file(self,name, content):
		with open(name, 'w') as target_file:
			target_file.write("%s" %content)

	def read_file(self, name):
		try:
			with open(name) as data_file:
				content = data_file.readline()
		except OSError:
			content = None
		return content

	def is_scheduled_update(self):
		target='scheduled'
		content=self.read_file(target)
		if content == "update":
			uos.remove(target)
			return True
		return False

	def schedule_update(self):
		target="scheduled"
		try:
			uos.remove(target)
		except Exception as e:
			pass
		self.write_file(target,"update")
	