# Freddy Osses Herrera
from utime import ticks_ms
from uos import chdir, getcwd
from gc import collect#, enable
from sys import print_exception
from ota.core import Firmware
from machine import deepsleep, WDT, reset, reset_cause

#enable()
wdt = WDT(timeout=180000)
f = Firmware()
print(reset_cause())

def run():
	collect()
	from main import start
	start(wdt)
	
def cleanmemory():
	for element in dir():
		if element[0:2] != "__" and not element in ("f","collect","updatefw","reset","todeepsleep","disconnectwifi","ticks_ms","deepsleep","print_exception"):
			del globals()[element]
	collect()

def updatefw(schedule=False):
#	wdt.feed()
	try:
		status=f.update(schedule)
		if status:
			twking=ticks_ms()
			f.write_file('time_update', twking)
	except Exception as e:
		status=None
		print_exception(e)
		print("Impossible to check updates because of %s" %repr(e))
	return status
	
def disconnectwifi():
	from network import WLAN
	wlan=WLAN(0)
	wlan.disconnect()
	print("Wi-Fi network disconnected")
	
def todeepsleep(tmetoslp, keep=False):
	disconnectwifi()
	twking=ticks_ms()
	print('ESP32 in deep sleep by %d msecs' %(tmetoslp-twking))
	deepsleep(tmetoslp-twking)
	
if __name__ == '__main__':
	if f.is_scheduled_update():
		updatefw()
		try:
			time_update=int(f.read_file('time_update'))
		except OSError:
			time_update=0
		twking=ticks_ms()
		print("time_update: %s" %time_update)
		todeepsleep(600000-time_update)
	else:
		try:
			chdir('firmware')
		except Exception as e:
			updatefw()
			disconnectwifi()
			reset()
		try:
			print(getcwd())
			run()
		except Exception as e:
			print_exception(e)
			'''if not stat_update:
				print("Fail to execution of the firmware. Executing backup firmware...")
				uos.chdir('/backup')
				run()
			else:
				'''
			print ("Fail to execution of the firmware \n Verifying Firmware Integrity...")
			if(f.verify()):
				print("Rollback to backup firmware")
				f.restore_backup()
			else:
				updatefw()
			reset()
		
#	if reset_cause() is not 5:
	chdir('/')
#	collect()
	cleanmemory()
	print("Verifying if exist new firmware updates...")
	if updatefw(schedule=True):
		reset()
	todeepsleep(600000)