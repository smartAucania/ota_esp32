# Actualizaciones Over The Air para ESP32!

Aplicación para actualizar de forma remota una aplicación alojada en la memoria flash del microcontrolador ESP32 .




# Caracteristicas

El software posee las siguientes características:

- Watchdog para reiniciar la máquina en caso de algún cuelgue con el software.
- Consulta de disponibilidad de actualización por cada ciclo de trabajo.
- Respaldo de versión antigua de código en caso de actualización corrupta.
- Rollback de código en caso de fallo en ejecución de código de ultima actualización.
- Limpieza de memoria flash al realizar una actualización, borrando la version anterior a la ultima versión estable.
- Guardado de tiempo utilizado para actualizar con el fin de restarlo del tiempo que se destinará a estar en modo deep sleep.
- 2 carpetas de trabajo: Firmware y Backup.
- Compatibilidad sólo con repositorios públicos de GitHub.



# Archivos de configuración
Existen un archivos de configuración dentro de la memoria flash y la memoria SD, son los siguientes: 
-ota_conf.json (flash)
-wifi.json (SD)

## ota_conf.json
Este archivo contiene la configuración para que el dispositivo acceda a un repositorio remoto de forma exitosa. Sus campos son los siguientes: 

|    Campo |Contenido  |                                            
|----------------|-------------------------------|
|"provider":| Host del repositorio. Solo GitHub  |
|"owner":| dueño del repositorio | 
|"repo":| Nombre del repositorio |
|"root_dir":| Carpeta donde se aloja el código de la app a actualizar | 

 ## wifi.json
Este archivo contiene la configuración para que el dispositivo se conecte a una red WiFi de forma exitosa. Sus campos son los siguientes: 

|    Campo |Contenido  |                                            
|----------------|-------------------------------|
|"ssid":| SSID de la red a la cual se desea conectar  |
|"pssw":| Contraseña para la red  con el SSID establecido en el campo anterior |
