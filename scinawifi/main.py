import network
from utime import sleep
from ujson import dumps
from uos import listdir, chdir
from microWebSrv import MicroWebSrv

message="Aun no se ha configurado ni probado la red"
okconnect=False

def get_id():
	from machine import unique_id
	x = [hex(int(c)).replace("0x","") for c in unique_id()]
	for i in range(len(x)):                                                                                                                           
		if len(x[i]) == 1:
			x[i] = "0"+x[i]
	return ''.join(x)

def do_connect(ssid,password):
	import network
	wlan = network.WLAN(network.STA_IF) # create station interface
	wlan.active(False)
	wlan.active(True)	   # activate the interface
	wlan.config(dhcp_hostname="ESP32_NODO_" + get_id())
	if wlan.isconnected():
		wlan.disconnect()
	if not wlan.isconnected():	  # check if the station is connected to an AP
		wlan.connect(ssid, password) # connect to the AP (Router)
		for _ in range(30):
			if wlan.isconnected():	  # check if the station is connected to an AP
				print('\nNetwork config:', wlan.ifconfig())
				return True
				# webrepl.start()
#				import uftpd
				break
			print('.', end='')
			sleep(1)
		else:
			print("\nConnect attempt timed out\n")
			return False
	else:
		print("Already connected")
		print('\nNetwork config:', wlan.ifconfig())
		return True

def saveData(data_file):
	with open('/fc/wifi.json', 'w') as target_file:
		target_file.write(data_file)
	content = dumps({
	"message": "SSID y contraseña actualizados"
	})
	return content

def startAP():
	# pass
	SSID = 'SCINABOX'
	PASSWORD = 'wslabufro'
	ap = network.WLAN(network.AP_IF)
	ap.active(True)
	ap.config(essid=SSID, authmode=network.AUTH_WPA_WPA2_PSK, password=PASSWORD, hidden=False)

	while ap.active() == False:
	  pass

	print('AP started succesfully')
	print(ap.ifconfig())
	
@MicroWebSrv.route('/wifi/network', 'POST')
def _httpHandlerTestPost(httpClient, httpResponse) :
	global message
	global okconnect
	formData  = httpClient.ReadRequestContentAsJSON()
	print("Received data: ")
	print(formData)
	okconfig=False
	if "ssid" in formData and "password" in formData:
		ssid = formData["ssid"]
		password  = formData["password"]
		print("Correct data")
		data_file=dumps({"ssid": ssid, "password": password})
		print(listdir())
		if not "fc" in listdir():
			print("not fc in listdir")
			try:
				from machine import SDCard
				from uos import mount
				_sd = SDCard(slot=2, width=1, sck=14, miso=15, mosi=13, cs=5)
				mount(_sd, "/fc")
				print("SD mounted")
				content = saveData(data_file)
				okconfig=True
			except Exception as e:
				repr(e)
				print("SD not mounted")
				content = None
				_sd.deinit()
				del _sd
			if content is None:
				content = dumps({
					"message": "SD no detectada. Requiere FAT32 y tam de cluster predeterminado"
				})
				print("No SD!")
		else:
			print("Saving WiFi data into SD")
			content=saveData(data_file)
			okconfig=True
	else:
		content = dumps({
			"message": "Faltan parametros"
		})
		print("Incomplete parameters")
	print("Sending answer to client")
	httpResponse.WriteResponseOk( headers		 = None,
								contentType	 = "application/json",
								contentCharset = "UTF-8",
								content 		 = content )
	if okconfig:
		sleep(1)
		# from boot import do_connect
		# srv.Stop()
		if do_connect(ssid,password):
			message= "Conexion exitosa a red Wi-Fi. Reiniciando estación en modo produccion"
			print("Conexion exitosa a red Wi-Fi. Volviendo a AP")
			okconnect=True
		else:
			message= "Conexión fallida a red Wi-Fi. Compruebe los parametros de red"
		startAP()
		# srv.Start()

@MicroWebSrv.route('/wifi/status','GET')
def handlerFuncGet(httpClient, httpResponse) :
	print("GET de estado de Wifi recibido")
	global message
	global okconnect
	content = dumps({
			"message": message
		})
	httpResponse.WriteResponseOk( headers		 = None,
								contentType	 = "application/json",
								contentCharset = "UTF-8",
								content 		 = content )
	print(content)
	if okconnect:
		sleep(5)
		from machine import reset
		reset()
		
chdir("/")
startAP()
srv = MicroWebSrv(webPath='/scinawifi/www/')
print("Web server starting")
srv.Start()
