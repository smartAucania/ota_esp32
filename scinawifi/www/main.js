window.addEventListener("load", (event)=>{
    const submitBtn = document.getElementById("submit-btn");
    const ssidInput = document.getElementById("ssid");
    const passInput = document.getElementById("password");
    const alertDiv  = document.getElementsByClassName("alert")[0];
    const spinner   = document.getElementsByClassName("loader")[0];
    let interval    = null;

    submitBtn.addEventListener("click", (event)=>{
        event.preventDefault();
        //console.log("submit");
        //console.log("SSID: "+ssidInput.value);
        //console.log("Password: "+passInput.value);

        if(ssidInput.value.length < 1){
            //console.log("El ssid debe ser ingresado");
            alertDanger("ssid debe ser ingresado");
            return;
        }

        if(passInput.value.length < 1){
            //console.log("La contraseña debe ser ingresada");
            alertDanger("La contraseña debe ser ingresada");
            return;
        }

        let data = {
            'ssid': ssidInput.value,
            'password': passInput.value
        };

        hideAlert();

        submitBtn.classList.remove("non-visible");
        submitBtn.classList.add("non-visible");
        spinner.classList.remove("non-visible");

        fetch('/wifi/network', {
            method: 'POST',
            body: JSON.stringify(data)
        }).then(function(response) {
            if(response.ok) {
                return response.json();
            } else {
                throw "Error en la llamada Ajax";
            }
        }).then(function(obj) {
            if(obj.message && obj.message == "SSID y contraseña actualizados"){
                //console.log("Bien hecho :D");
                alertSuccess("Red wifi configurada con exito! Testeando conexión...");
                submitBtn.disabled = true;
                interval = setInterval(()=>{
                    fetch('/wifi/status')
                        .then((response)=>{
                            let resp = response.json();
                            
                            return resp;
                        })
                        .then((resp)=>{
                            console.log(resp);
                            if(resp && resp.message){
                                if (resp.message == "Conexion exitosa a red Wi-Fi. Reiniciando estación en modo produccion"){
                                    alertSuccess(resp.message);
                                    clearInterval(interval);
                                }else{
                                    alertDanger(resp.message);
                                    if(resp.message == "Conexión fallida a red Wi-Fi. Compruebe los parametros de red"){
                                        submitBtn.disabled = false;
                                        clearInterval(interval);
                                    }
                                }
                            }
                            console.log(resp);
                        })
                        .catch((err)=>{
                            console.log(err);
                        });
                }, 2000);
            }else if(obj.message){
                alertDanger(obj.message);
            }else{
                //console.log("Algo salio mal :c");
                alertDanger("Falla inesperada");
            }
            spinner.classList.remove("non-visible");
            spinner.classList.add("non-visible");
            submitBtn.classList.remove("non-visible");
            
        }).catch(function(err) {
            //console.log(err);
            alertDanger(err);
            spinner.classList.remove("non-visible");
            spinner.classList.add("non-visible");
            submitBtn.classList.remove("non-visible");
        });
    });

    function alertDanger(text){
        alertDiv.firstElementChild.textContent = text;
        alertDiv.classList.remove("alert-success");
        if(!alertDiv.classList.contains("alert-danger")){
            alertDiv.classList.add("alert-danger");
        }
        alertDiv.classList.remove("non-visible");
    }

    function alertSuccess(text){
        alertDiv.firstElementChild.textContent = text;
        alertDiv.classList.remove("alert-danger");
        if(!alertDiv.classList.contains("alert-success")){
            alertDiv.classList.add("alert-success");
        }
        alertDiv.classList.remove("non-visible");
    }

    function hideAlert(){
        alertDiv.classList.remove("alert-danger");
        alertDiv.classList.remove("alert-success");
        alertDiv.firstElementChild.textContent = "";

        alertDiv.classList.remove("non-visible");
        alertDiv.classList.add("non-visible");
    }
});