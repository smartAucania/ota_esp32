# This file is executed on every boot (including wake-boot from deepsleep)
# Freddy Osses Herrera
from gc import collect
from esp import osdebug
from time import sleep
from ujson import load
import webrepl
from uos import mount, umount, chdir
from machine import unique_id, SDCard

osdebug(None)
collect()

_sd = None
try:
	_sd = SDCard(slot=2, width=1, sck=14, miso=15, mosi=13, cs=5)
	mount(_sd, "/fc")
	SD=True
	print("SD mounted")
except Exception as e:
	_sd.deinit()
	del _sd
	SD=False
	print("SD not mounted")
	
def readwificfg(): 
	try:
		FILE_NAME = "/fc/wifi.json"
		with open(FILE_NAME) as json_data_file:
			data = load(json_data_file)
		return data
	except Exception as e:
		print("Error reading Wi-Fi data because " +repr(e))
		return None
		
def get_id():
	x = [hex(int(c)).replace("0x","") for c in unique_id()]
	for i in range(len(x)):                                                                                                                           
		if len(x[i]) == 1:
			x[i] = "0"+x[i]
	return ''.join(x)
  
def do_connect(ssid,password):
	import network
	wlan = network.WLAN(network.STA_IF) # create station interface
	wlan.active(True)	   # activate the interface
	wlan.config(dhcp_hostname="NODO_" + get_id())
	if not wlan.isconnected():	  # check if the station is connected to an AP
		wlan.connect(ssid, password) # connect to the AP (Router)
		for _ in range(30):
			if wlan.isconnected():	  # check if the station is connected to an AP
				print('\nNetwork config:', wlan.ifconfig())
				webrepl.start()
#				import uftpd
				break
			print('.', end='')
			sleep(1)
		else:
			print("\nConnect attempt timed out\n")
			return False
	else:
		print("Already connected")
		print('\nNetwork config:', wlan.ifconfig())
		return True
	
wficfg=None
if SD:
	print("Reading Wi-Fi data from SD")
	wficfg=readwificfg()
	if wficfg is not None:
		do_connect(wficfg["ssid"], wficfg["password"])
	
if not SD or wficfg is None:
	print("Starting scinawifi...")
	chdir("/scinawifi")
	from sys import exit
	exit()
if SD and wficfg is not None:
	umount('/fc')
	print("SD unmounted")
del osdebug, sleep, load, mount, umount, unique_id, SDCard,SD, wficfg
collect()